<?php
require_once "logincheck.php";
require_once "functions.php";

$audi_id = '9542018936806c44c9e70be0bc37ba07a129d4b0e3783ef07acbb7a839381514';
$audi = new Auditorium();
$audi->__set('audi_id', $audi_id);
$a = $audi->getEntryStatus();
$entry = $a[0]['entry'];
if (!$entry) {
    header('location: lobby.php');
}
$curr_room = 'auditorium1';
//$webcastUrl = 'https://vimeo.com/event/928425/embed/c80ddc011f';
$webcastUrl = '';

$sess_id = 0;
if (isset($_GET['ses'])) {
    $sess_id = $_GET['ses'];
    $sess = new Session();
    $sess->__set('session_id', $sess_id);
    $curr_sess = $sess->getSession();
    if ((empty($curr_sess)) || (!$curr_sess[0]['launch_status'])) {
        header('location: auditorium1.php');
    }

    $webcastUrl = $sess->getWebcastSessionURL();
    // $webcastUrl .= '?autoplay=1';
} else {
    $webcastUrl .= '?autoplay=1&loop=1';
}
?>
<?php require_once 'header.php';  ?>

<?php require_once 'preloader.php';  ?>

<div class="page-content">
    <div id="content">
        <div id="header-menu">
            <?php require_once "header-navmenu.php" ?>
        </div>
        <div id="bg">
            <img src="assets/folder/AW-1 F-1.jpg">
            <div id="webcast-area" class="audi1">
                <!-- <a id="goFS" href="#" class="fs">Fullscreen</a> -->
                <iframe src="<?= $webcastUrl ?>" frameborder="0" allow="autoplay; fullscreen" allowfullscreen style="width:100%;height:100%;"></iframe>
            </div>
        </div>
        <!-- <div id="disclaimer">
            Integrace Pvt Ltd has obtained the necessary consent from the respective copyright holders for relaying, sharing & archiving the materials appearing in this program. However, Integrace Pvt Ltd accepts no role or liability for the medical opinion, accuracy or completeness of the information contained in this publication or its update.
        </div> -->
        <!-- <div id="audiAgenda">
            <a id="showaudiAgenda" href="#"><i class="far fa-list-alt"></i>Agenda</a>
        </div> -->
        <!-- <div id="audiAgenda">
            <a id="" href="assets/folder/HELPDESK F-2.jpg" class="view"><i class="far fa-list-alt"></i>Agenda</a>
        </div> -->
        <?php
        if ($sess_id != '0') {
        ?>
            <div id="ask-ques">
                <a href="#" id="askques"><i class="fas fa-comment-alt"></i>Comments</a>
            </div>
            <!-- <div id="ask-ques">
                <a href="#" id=""><i class="fas fa-question-circle"></i>Ask Ques</a>
            </div> -->
            <!-- <div id="take-poll">
                <a href="#" id="takepoll"><i class="fas fa-poll"></i>Take Poll</a>
            </div> -->
            <!-- <div id="take-poll">
                <a href="#" id=""><i class="fas fa-poll"></i>Take Poll</a>
            </div> -->
            <div class="panel ques">
                <div class="panel-heading">
                    Comments
                    <a href="#" class="close" id="close_ques"><i class="fas fa-times"></i></a>
                </div>
                <div class="panel-content">
                    <div id="ques-message" style="display:none;"></div>
                    <form>
                        <div class="form-group">
                            <textarea class="input" rows="6" name="userques" id="userques" required></textarea>
                        </div>
                        <div class="form-group">
                            <button type="button" name="send_sesques" data-ses="<?= $sess_id ?>" data-user="<?= $userid ?>"  class="send_sesques btn btn-sm btn-primary btn-sendmsg">Submit</button>
                        </div>
                    </form>
                                    </div>
 <div id="askedQues1" class="scroll">
                  <?php
     $servername = "localhost";
     $username = "coacteh9_coact";
    $password = "Coact@2020#";
   $dbname = "oppo_feb2022";

$conn = mysqli_connect($servername, $username, $password, $dbname);

if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}

//$sql = "SELECT id, userid, question FROM tbl_sessionquestions";
 $sql= "SELECT tbl_sessionquestions.userid, tbl_users.emailid, tbl_sessionquestions.question FROM tbl_sessionquestions
 INNER JOIN tbl_users ON tbl_sessionquestions.userid=tbl_users.userid";

$result = mysqli_query($conn, $sql);



if (mysqli_num_rows($result) > 0) {
    
    while($row = mysqli_fetch_assoc($result)) {
        echo $row["emailid"].":". "<br>";
        echo $row["question"]. "<br>";
        
        // $_SESSION['user_name'].
    }
} else {
    echo "results";
}

mysqli_close($conn);
?>
       
</div> 

            </div>
            <div class="panel poll">
                <div class="panel-heading">
                    Take Poll
                    <a href="#" class="close" id="close_poll"><i class="fas fa-times"></i></a>
                </div>
                <div class="panel-content">
                    <div id="poll-message" style="display:none;"></div>
                    <div id="currpollid" style="display:none;">0</div>
                    <div id="currpoll" style="display:none;"></div>
                    <div id="currpollresults" style="display:none"></div>
                </div>
            </div>
        <?php
        }
        ?>
        <div id="bottom-menu">
            <?php require_once "bottom-navmenu.php" ?>
        </div>
    </div>
</div>
<section class="videotoplay" id="gotoaudi1" style="display:none;">
    <video class="videoplayer" id="gotoaudi1video" preload="auto">
        <source src="toaudi.mp4" type="video/mp4">
    </video>
    <a href="auditorium1.php" class="skip">SKIP</a>
</section>

<?php require_once "commons.php" ?>
<?php require_once "scripts.php" ?>
<?php require_once "audi-common.php"
?>
<?php require_once "audi-script.php" ?>
<?php require_once "ga.php"; ?>
<?php require_once 'footer.php';  ?>

<script>
    var audi1Video = document.getElementById("gotoaudi1video");
    audi1Video.addEventListener('ended', audi1End, false);
    function enterAudi1() {
        $('#content').css('display', 'none');
        $('#gotoaudi1').css('display', 'block');
        audi1Video.currentTime = 0;
        audi1Video.play();
    }
</script>
<style>
     div.scroll {
    margin: 4px, 4px;
    padding: 12px;
    background-color: aliceblue;
    width: 270px;
    height: 310px;
    overflow-x: hidden;
    overflow-y: auto;
    font-size: 18px;
    margin-left: 10px;
}
    </style>