<style>
    .modal-tabs li {
      font-size:11px;
      font-weight: bolder;
}
</style>


<div class="modal fade" id="confAgenda" tabindex="-1" role="dialog" aria-labelledby="confAgendaabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="confAgendaabel">Agenda</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" class="white-text">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="agendaItems" class="content scroll">
                <ul class="modal-tabs nav nav-tabs" role="tablist">
                        <li id="day1" class="active">
                            <a href="assets/resources/90thSBCI day1.pdf" class="showpdf" id="showAgenda">
                            16th December 2021
                            </a>
                        </li>
                        <li id="day2" class="">
                            <a href="assets/resources/90th SBCI day2.pdf" class="showpdf" id="showAgenda">
                            17th December 2021
                            </a>
                        </li>
                        <li id="day3" class="">
                            <a href="assets/resources/90th SBCI day3.pdf" class="showpdf" id="showAgenda">
                            18th December 2021
                            </a>
                        </li>
                        <li id="day3" class="">
                            <a href="assets/resources/90th SBCI day4.pdf" class="showpdf" id="showAgenda">
                            19th December 2021
                            </a>
                           
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="day1Agenda" style="display:block;">

                        </div>
                        <div class="tab-pane" id="day2Agenda" style="display:none;">

                        </div>
                        <div class="tab-pane" id="day3Agenda" style="display:none;">

                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>