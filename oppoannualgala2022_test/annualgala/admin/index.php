<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Admin Login : Live Webcast</title>
<link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="../css/styles.css" rel="stylesheet" type="text/css">
</head>

<body class="admin">
<nav class="navbar navbar-expand-lg navbar-light">
  <a class="navbar-brand" href="#"><img src="../img/OPPOLOGO.jpg" class="logo"></a>
</nav>
<div class="container-fluid main">
    <div class="row mt-5">
        <div class="col-8 col-md-6 col-lg-4 offset-2 offset-md-3 offset-lg-4">
            <div class="login">
                <h3>Admin Login</h3>
                <form id="login-form" method="post" role="form">
                  <div id="login-message"></div>
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Username" aria-label="Userame" aria-describedby="basic-addon1" name="loginUser" id="loginUser" required>
                  </div>
                  
                  <div class="input-group">
                    <input type="password" class="form-control" placeholder="Password" aria-label="Password" aria-describedby="basic-addon1" name="loginPwd" id="loginPwd" required>
                  </div>
                  
                  <div class="input-group">
                    <button id="login" class="btn btn-primary btn-sm login-button" type="submit">Login</button>
                  </div>
                
            </form>
            </div>
        
        </div>
    </div>
</div>

<script src="../js/jquery.min.js"></script>
<script>
$(function(){

  
  
  $(document).on('submit', '#login-form', function()
{  
  $.post('chkforlogin.php', $(this).serialize(), function(data)
  {
      
      if(data=="0")
      {
        $('#login-message').text('Invalid login. Please try again.');
        $('#login-message').addClass('alert-danger');
      }
      else if(data =='s')
      {
        window.location.href='users.php';  
      } else
      {
        $('#login-message').text(data);
        $('#login-message').addClass('alert-danger');
      }
      
  });
  
  return false;
});

});

</script>
</body>
</html>