<?php
	require_once "config.php";
	
	if(!isset($_SESSION["email"]))
	{
		header("location: index.php");
		exit;
	}
	
	if(isset($_GET['action']) && !empty($_GET['action'])) 
    {
        $action = $_GET['action'];
        if($action == "logout")
        {
            $logout_date   = date('Y/m/d H:i:s');
          
            $email=$_SESSION["email"];
        
            $query="UPDATE tbl_users set logout_date='$logout_date', logout_status='0' where email='$email'";
            $res = mysqli_query($link, $query) or die(mysqli_error($link));

            unset($_SESSION["user_name"]);
            unset($_SESSION["email"]);
         
            
            header("location: ./");
            exit;
        }

    }
	
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>OPPO Annualgala</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/styles.css">
</head>

<body>
<div class="container-fluid">    
    <div class="row ">
        <div class="col-12  mt-2" >
        <img src="img/livelogo.png" alt="" class="img_width"  style="width:100px; ">

        <!-- <img src="img/2.jpg" alt="" class="offset-md-1 mb-1" style="width:50px; height:100px">
        <img src="img/3.jpg" alt=""  class="offset-md-1 mb-1" style="width:50px; height:100px">
        <img src="img/4.jpg" alt=""  class="offset-md-1 mb-1" style="width:50px; height:100px">
        <img src="img/5.jpg" alt="" class="offset-md-1 mb-1" style="width:50px; height:100px"> -->
        <div class="text-right  " style="color: white;">
                Hello <?php echo $_SESSION['user_name']; ?>! <a href="?action=logout" class="btn btn-sm btn-danger ">Logout</a>
            </div>
        </div>
    </div>
    <div class="row ">
        <div class="col-12 col-md-6 mt-2 offset-md-4" >
       
        <img src="img/slogan_text.png" alt="" class=" mb-3 img-fluid" >
      
        </div>
    </div>
    <div class="row ">
        <div class="col-12 col-md-8 offset-md-2 text-center">
            
            <div class="video-panel">
            <div class="embed-responsive embed-responsive-16by9 ">
            <iframe src="video.php" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen style="position:absolute;top:0;left:0;width:100%;height:100%;"></iframe>
            </div>
            </div>
        </div>
        <div class="col-12 col-md-6  text-center mt-2 d-none">
            <div id="question" class="mt-0 offset-md-2" style="width:70%;">
              <div id="question-form" class="panel panel-default">
                  <form method="POST" action="#" class="form panel-body" role="form">
                      <div class="row">
                          <div class="col-12">
                          <div id="ques-message"></div>
                          <div class="form-group">
                              <textarea class="form-control" name="userQuestion" id="userQuestion" required placeholder="Please ask your question" rows="2"></textarea>
                          </div>
                          
                          </div>
                          <div class="col-12">
                          <input type="hidden" id="user_name" name="user_name" value="<?php echo $_SESSION['user_name']; ?>">
                          <input type="hidden" id="email" name="email" value="<?php echo $_SESSION['email']; ?>">
                        
                          <button class="btn btn-primary btn-sm btn-submit" type="submit">Submit your Question</button>
                          </div>
                      </div>
                      
                      
                      
                  </form>

                 
          </div>
     
        </div>
    </div>
</div>
<div class="container-fluid d-none">    
    <div class="row mt-3 mb-1">
        <div class="col-12 text-center p-0">
       
          <img src="img/slogan1.png" alt="" class="img-fluid">
        
        </div>
    </div>
</div>
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script>
$(function(){


	$(document).on('submit', '#question-form form', function()
    {  
            $.post('submitques.php', $(this).serialize(), function(data)
            {
                if(data=="success")
                {
                  $('#ques-message').text('Your question is submitted successfully.');
                  $('#ques-message').removeClass('alert-danger').addClass('alert-success').fadeIn().delay(2000).fadeOut();
                  $('#question-form').find("textarea").val('');
                }
                else 
                {
                  $('#ques-message').text(data);
                  $('#ques-message').removeClass('alert-success').addClass('alert-danger').fadeIn().delay(5000).fadeOut();
                }
                
            });
        
      
      return false;
    });
});
function update()
{
    $.ajax({ url: 'ajax.php',
         data: {action: 'update'},
         type: 'post',
         success: function(output) {
			   if(output=="0")
			   {
				   location.href='index.php';
			   }
         }
});
}
setInterval(function(){ update(); }, 30000);

function changeVideo(video, l)
{
    var vid = '.'+l;
    $('.vid-link').removeClass('act');
    $(vid).addClass('act');
    
    $('#webcast').attr("src",video);
    
    return false;
    
}

</script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-93480057-11"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-93480057-11');
</script>

</body>
</html>