<?php
require_once "config.php";
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>OPPO Annualgala</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/styles.css">
<script src="https://kit.fontawesome.com/e8d81f325f.js" crossorigin="anonymous"></script>
</head>

<body>
<div class="container-fluid">    
    <div class="row ">
        <div class="col-12" >
        <img src="img/livelogo.png" alt="" style="width:200px;">
        </div>
    </div>
</div>
<div class="container">    
    <div class="row ">
        <div class="col-12 col-md-6 offset-md-3 text-center mt-5 p-5">
            <form id="login-form" method="post">
            <h1>Login</h1>
              <div id="login-message"></div>
              
              <div class="input-group mt-1 mb-1">
                <input type="text" class="form-control" placeholder="Name" aria-label="Name" aria-describedby="basic-addon1" name="name" id="name" required>
              </div>
             
              <div class="input-group mt-1 mb-1">
                <input type="email" class="form-control" placeholder="Email" aria-label="Phone Number" aria-describedby="basic-addon1" name="email" id="email" required>
              </div>
              <div class="input-group mt-1 mb-1">
                <button class="mt-4 btn btn-block" type="submit">Login</button>
              </div>
            </form>
        </div>
    </div>
</div>
<!-- <div class="container-fluid">    
    <div class="row mt-5 mb-2 p-0">
        <div class="col-12 col-md-12 text-center p-0">
            <div class="icons bg-black">
            <a href="https://www.facebook.com/TheFreedomFromDiabetes" target="_blank"><img src="img/036-facebook.svg" alt=""/></a><a href="https://www.youtube.com/user/FreedomFromDiabetes" target="_blank"><img src="img/001-youtube.svg" alt=""/></a><a href="https://www.freedomfromdiabetes.org/" target="_blank" class="web"><img src="img/web.svg" alt=""/></i></a>
            </div>
        </div>
    </div>
</div> -->

<div class="container-fluid">    
    <div class="row mt-3 mb-1">
        <div class="col-12 text-center p-0">
       
          <img src="img/slogan1.png" alt="" class="img-fluid">
        
        </div>
    </div>
</div>
<script src="js/jquery.min.js"></script>
<script>
$(function(){

  // $('.input').focus(function(){
  //   $(this).parent().find(".label-txt").addClass('label-active');
  // });

  // $(".input").focusout(function(){
  //   if ($(this).val() == '') {
  //     $(this).parent().find(".label-txt").removeClass('label-active');
  //   };
  // });
  
  $(document).on('submit', '#login-form', function()
{  

    if($('#email').val() == '-1')
    {
        alert('Please select email id');
        return false;
    }
  $.post('chkforlogin.php', $(this).serialize(), function(data)
  {
      
      if(data=="-1")
      {
        $('#login-message').text('You are already logged in. Please logout from other Email ID  and try again.');
        $('#login-message').addClass('alert-danger');
      }
      else 
      if(data=="0")
      {
        $('#login-message').text('Your phone numer is not registered. Please register.');
        $('#login-message').addClass('alert-danger');
      }
      else if(data =='s')
      {
        window.location = 'webcast.php';   
      }
      
  });
  
  return false;
});

});

</script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-93480057-11"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-93480057-11');
</script>

</body>
</html>