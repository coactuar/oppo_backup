<?php
require_once "config.php";
require_once "logincheck.php";
$curr_room = 'games';

?>
<?php require_once 'header.php';  ?>
<?php

// require_once 'preloader.php';

?>

<div class="page-content">
    <div id="content">
        <div id="header-menu">
            <?php require_once "header-navmenu.php" ?>
        </div>
        <div id="bg">
            <img src="assets/folder/GAMING ZONE.jpg">
            <a href="games/game/game/index.php" id="enterCricket">
                <!-- <img src="assets/img/car-race.jpg" class="img-fluid" alt="Car Race"> -->
                <div class="indicator d-10"></div>
            </a>
            <a href="games/game1/game/index.php" id="enterCar">
                <!-- <img src="assets/img/car-race.jpg" class="img-fluid" alt="Car Race"> -->
                <div class="indicator d-10"></div>
            </a>
            <!-- <a href="" > -->
                <!-- <img src="assets/img/car-race.jpg" class="img-fluid" alt="Car Race"> -->
                <button type="button" class="btn btn-primary" id="enterLeader" data-toggle="modal" data-target="#myModal">
                    <!-- Open modal -->
                </button>
                <!-- <div class="indicator d-6"></div> -->
            <!-- </a> -->
            <!-- <div id="games-area">
                <div class="row d-flex">
                    <div class="col-3 mx-auto mt-2">
                        <div class="bg-white p-2 rounded m-1 h-100">
                            <a href="games/sudoku/" class="playGames" onclick="gamescore('sudoku')">
                                <img src="assets/img/sudoku.jpg" class="img-fluid" alt="Sudoku">
                            </a>
                            <h6 class="text-dark">Sudoku</h6>
                        </div>
                    </div>
                    <div class="col-3 mx-auto mt-2">
                        <div class="bg-white p-2 rounded m-1 h-100">
                            <a href="games/image-puzzle/" class="playGames" onclick="gamescore('image-puzzle')">
                                <img src="assets/img/brand-puzzle.png" class="img-fluid" alt="Brand Puzzle">
                            </a>
                            <h6 class="text-dark">Brand Puzzle</h6>
                        </div>
                    </div>
                    <div class="col-3 mx-auto mt-2 d-none d-md-block">
                        <div class="bg-white p-2 rounded m-1 h-100">
                            <a href="games/car-race/" class="playGames" onclick="gamescore('car-race')">
                                <img src="assets/img/car-race.jpg" class="img-fluid" alt="Car Race">
                            </a>
                            <h6 class="text-dark">Car Race</h6>
                        </div>
                    </div>
                    <div class="col-3 mx-auto mt-2">
                        <div class="bg-white p-2 rounded m-1 h-100">
                            <a href="games/word-search/" class="playGames" onclick="gamescore('word-search')">
                                <img src="assets/img/word-game.jpg" class="img-fluid" alt="Word Search">
                            </a>
                            <h6 class="text-dark">Word Search</h6>
                        </div>
                    </div>

                </div>

            </div> -->

        </div>
        <div class="modal" id="myModal">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Leaderboard</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">


            <table class="table">
                <thead>
                    <tr>
                    <!-- <th scope="col">#</th> -->
                    <th scope="col">Username</th>
                    <th scope="col">Score</th>
                    <!-- <th scope="col">Handle</th> -->
                    </tr>
                </thead>
                <tbody>
                <?php		
                         
                         $query="SELECT USERNAME, EMAIL_ID,SCORE FROM oppo_feb2022.MASTER_LEADERBOARD";
                         $res = mysqli_query($link, $query) or die(mysqli_error($link));
                         while($data = mysqli_fetch_assoc($res))
                         {
                         ?>
                           <tr class="color_change">
                             <td><?php echo $data['USERNAME']; ?></td>
                             <td><?php echo $data['SCORE']; ?></td>
 
                           </tr>
                       <?php			
                         }
                       ?>
                </tbody>
            </table>
        </div>
        
        <!-- Modal footer -->
        <!-- <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div> -->
        
      </div>
    </div>
  </div>
  
        <div id="bottom-menu">
            <?php require_once "bottom-navmenu.php" ?>
        </div>
    </div>
    <?php require_once "commons.php" ?>
</div>

<?php require_once "scripts.php" ?>
<script>
    function gamescore(a) {
        $.ajax({
            url: 'control/lb.php',
            data: {
                action: 'updpoints',
                userId: '<?= $_SESSION['userid'] ?>',
                activity: 'PLAY_GAME',
                loc: a
            },
            type: 'post',
            success: function(message) {}
        });
        //window.open('games/' + a + '', '_blank');
    }
</script>

<?php require_once "ga.php"; ?>

<?php require_once 'footer.php';  ?>