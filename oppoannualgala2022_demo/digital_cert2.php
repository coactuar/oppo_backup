<style>
    .xyz{
        font-weight: bolder;
    }
</style>
<?php
//require_once "logincheck.php";
require_once "logincheckprereg.php";
$curr_room = 'digital_cert';
?>
<?php require_once 'header.php';  ?>
<?php require_once 'preloader.php';  ?>

<div class="page-content">
    <div id="content">
        <div id="header-menu">
            <?php require_once "header-navmenu.php" ?>
        </div>
        <div id="bg">
         <p class="xyz text-dark text-center bg-warning">YOU CAN CLICK ANYWHERE ON THE IMAGE OR BUTTON TO DOWNLOAD THE CERTIFICATE</p>
            <img src="assets/img/amity-university-office.jpg">
            <div id="cert-area">
                <div class="cert">
                    <a href="#" onclick="dlCert()"><img class="photo-jacket" src= <?=$cert?> /></a>
                    <div class="name-text"><?= $user_name ?></div>
                    <div class="affiliation-text"><?= $user_affiliation ?></div>
                </div>
                <a href="#" id="dlCert" onclick="dlCert()">Download Certificate</a>
            </div>




        </div>
        <div id="bottom-menu">
            <?php require_once "bottom-navmenu.php" ?>
        </div>
    </div>
    <?php require_once "commons.php" ?>
</div>

<?php require_once "scripts.php" ?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/FileSaver.js/2.0.2/FileSaver.js"></script>
<script type="text/javascript" src="assets/js/html2canvas.min.js"></script>
<script>
    function dlCert() {
        html2canvas(document.querySelector("#cert-area"), {
            scale: 3,
            backgroundColor: "#000000"
        }).then(c => {
            c.toBlob(function(blob) {
                saveAs(blob, "<?= $user_name ?>_certificate.jpg");
            });
        });
    }
</script>
<?php require_once "ga.php"; ?>

<?php require_once 'footer.php';  ?>